import java.io.*;

public class IoBufferReadWrite {
    public static void main(String[] args) {
        writeBuffer();
        readBuffer();
    }

    private static void writeBuffer() {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("filename.txt"))) {
            String fileContent = "This is a sample text.";
            bufferedWriter.write(fileContent);
        } catch (IOException e) {
            // Exception handling
        }    }

    private static void readBuffer() {
        // Read the content from file
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("filename.txt"))) {
            String line = bufferedReader.readLine();
            while(line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            // Exception handling
        } catch (IOException e) {
            // Exception handling
        }
    }
}
