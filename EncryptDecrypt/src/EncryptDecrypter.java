import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Locale;


public class EncryptDecrypter {

    public static void main(String[] args) {

        User user = new SerialiseDeserialiser().readObjectFromFile("file.ser");
        Base64.Encoder encoder = Base64.getEncoder();
        Base64.Decoder decoder = Base64.getDecoder();

        if(args[0].toLowerCase(Locale.ROOT).equals("-e")){
            byte[] byteArr = (user.id + "_" + user.name).getBytes(StandardCharsets.UTF_8);
            byte[] byteArr2 = encoder.encode(byteArr);
            NioFileReadWriter.writeFile(byteArr2, Paths.get(args[1]));

            if(args[2] != null && args[2].toLowerCase(Locale.ROOT).equals("-c")){
                System.out.println("Encoded string: "+ new String(byteArr2));
            }
        }

        if(args[0].toLowerCase(Locale.ROOT).equals("-d")){

            byte[] encodedByteArray = new byte[0];

            try {
                encodedByteArray = Files.readAllBytes(Paths.get(args[1]));
            } catch (IOException e) {
                e.printStackTrace();
            }

            String decodedString = new String(decoder.decode(new String(encodedByteArray)));

            if(args[2] != null && args[2].toLowerCase(Locale.ROOT).equals("-c")){
                System.out.println("Decoded string: "+ decodedString);
            }
        }
    }
}
