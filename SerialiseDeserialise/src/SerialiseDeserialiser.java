import java.io.*;

class SerialiseDeserialiser
{
    public static void main(String[] args)
    {
        User object = new User(1, "Name");
        String filename = "file.ser";

        writeObjectInFile(object, filename);

        User user = null;

        readObjectFromFile(filename);
    }

    static User readObjectFromFile(String filename) {
        User user;
        try
        {
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            user = (User)in.readObject();

            in.close();
            file.close();

            System.out.println("id: " + user.id);
            System.out.println("Name: " + user.name);

            return user;
        }

        catch(IOException ex)
        {
            System.out.println(ex.toString());
            return null;
        }

        catch(ClassNotFoundException ex)
        {
            System.out.println(ex.toString());
            return null;
        }
    }

    private static void writeObjectInFile(User object, String filename) {
        try
        {
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeObject(object);

            out.close();
            file.close();
        }

        catch(IOException ex)
        {
            System.out.println(ex.toString());
        }
    }
}